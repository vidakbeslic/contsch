<?php

use App\Http\Controllers\UserSettingController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('displays', Api\DisplayController::class);
Route::resource('appointments', Api\AppointmentController::class);
Route::resource('users', Api\UserController::class);
Route::resource('user/settings', Api\UserSettingController::class);
